package com.test.app.command

import com.test.app.AppContext

/** The base command interface. */
interface Command {
    /**
     * The method to execute command with given render.
     * @param appContext {@see AppContext}
     */
    fun execute(appContext: AppContext)

    /** The method to get arguments from incoming string line. */
    fun getArgsFromCommandLine(line: String): List<String> {
        return line.split(" ")
    }
}