package com.test.app.figure

import com.test.app.canvas.Canvas
import com.test.app.canvas.Point
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.util.*


/**
 * The canvas filling.
 * @param x x coords of start filling
 * @param y y coords of start filling
 */
class BucketFill(
        private val x: Int,
        private val y: Int,
        private val filler: String
) : Figure {

    /** {@inheritDoc} */
    override fun addToCanvas(canvas: Canvas) {
        runBlocking {
            val directions1 = listOf(up(canvas) { _ -> 0 }, down(canvas) { _ -> canvas.getHeight() })
            runFillWithDirection(x, y, directions1, left(canvas) { x -> x - 1 })
            runFillWithDirection(x, y, directions1, right(canvas) { x -> x + 1 })
            runFillWithDirection(x, y, directions1.reversed(), left(canvas) { x -> x - 1 })
            runFillWithDirection(x, y, directions1.reversed(), right(canvas) { x -> x + 1 })

            val directions2 = listOf(left(canvas) { _ -> 0 }, right(canvas) { _ -> canvas.getWidth() })
            runFillWithDirection(y, x, directions2, up(canvas) { y -> y - 1 })
            runFillWithDirection(y, x, directions2, down(canvas) { y -> y + 1 })
            runFillWithDirection(y, x, directions2.reversed(), up(canvas) { y -> y - 1 })
            runFillWithDirection(y, x, directions2.reversed(), down(canvas) { y -> y + 1 })
        }
    }

    private fun runFillWithDirection(
            sidestepInit: Int,
            directionInit: Int,
            directions: List<(Int, Int) -> Pair<Int, Boolean>>,
            sidestep: (Int, Int) -> Pair<Int, Boolean>
    ) {
        var sideStep = sidestepInit
        var direction = directionInit
        val directionsQeque = ArrayDeque<(Int, Int) -> Pair<Int, Boolean>>()
        directions.forEach { d ->
            directionsQeque.add(d)
        }
        GlobalScope.async() {
            var currentDirection = directionsQeque.pop()
            var isStepSideEnd = false
            var isDirectionEnd = false
            while (!(isStepSideEnd && isDirectionEnd)) {
                val directionData = currentDirection(direction, sideStep)
                val sidestepData = sidestep(sideStep, directionData.first)
                sideStep = sidestepData.first
                isStepSideEnd = sidestepData.second
                direction = directionData.first
                isDirectionEnd = directionData.second
                if (isDirectionEnd) {
                    directionsQeque.add(currentDirection)
                    currentDirection = directionsQeque.pop()
                }
            }
        }
    }

    /** The method to fill {@see Canvas} from current point to right. */
    private fun right(
            canvas: Canvas,
            end: (Int) -> Int
    ): (Int, Int) -> Pair<Int, Boolean> {
        return rightFun@{ x0: Int, y: Int ->
            val end0 = end(x0)
            for (w in x0 until end0)
                if (canvas.addPoint(Point(w, y, filler))) return@rightFun w - 1 to true

            end0 to false
        }
    }

    /** The method to fill {@see Canvas} from current point to up. */
    private fun up(canvas: Canvas, end: (Int) -> Int): (Int, Int) -> Pair<Int, Boolean> {
        return upFun@{ y0: Int, x: Int ->
            val end0 = end(y0)
            for (h in y0 downTo end0)
                if (canvas.addPoint(Point(x, h, filler))) return@upFun h + 1 to true

            end0 to false
        }
    }

    /** The method to fill {@see Canvas} from current point to left. */
    private fun left(
            canvas: Canvas,
            end: (Int) -> Int
    ): (Int, Int) -> Pair<Int, Boolean> {
        return leftFun@{ x0: Int, y: Int ->
            val end0 = end(x0)
            for (w in x0 downTo end0)
                if (canvas.addPoint(Point(w, y, filler))) return@leftFun w + 1 to true

            end0 to false
        }
    }

    /** The method to fill {@see Canvas} from current point to down. */
    private fun down(
            canvas: Canvas,
            end: (Int) -> Int
    ): (Int, Int) -> Pair<Int, Boolean> {
        return downFun@{ y0: Int, x: Int ->
            val end0 = end(y0)
            for (h in y0 until end0)
                if (canvas.addPoint(Point(x, h, filler))) return@downFun h - 1 to true

            end0 to false
        }
    }

    /** {@inheritDoc} */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BucketFill

        if (x != other.x) return false
        if (y != other.y) return false
        if (filler != other.filler) return false

        return true
    }

    /** {@inheritDoc} */
    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        result = 31 * result + filler.hashCode()
        return result
    }
}