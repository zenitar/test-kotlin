package com.test.app.canvas

import com.test.app.render.Render
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Test

import org.junit.Before
import kotlin.test.assertEquals

class ArrayCanvasTest {
    lateinit var canvas: ArrayCanvas

    @MockK
    lateinit var render: Render<Any>

    @MockK
    lateinit var renderFunction: (Any) -> Unit

    init {
        MockKAnnotations.init(this)
    }

    @Before
    fun setup() {
        canvas = ArrayCanvas(WIDTH, HEIGHT, render)
    }

    @Test
    fun getWidth() {
        assertEquals(WIDTH, canvas.getWidth(), "Canvas has wrong width")
    }

    @Test
    fun getHeight() {
        assertEquals(HEIGHT, canvas.getHeight(), "Canvas has wrong height")
    }

    @Test
    fun renderCanvas() {
        every { render.getRenderFunction() } returns renderFunction
        every { renderFunction.invoke(any()) } returns Unit
        canvas.renderCanvas()
        verify(exactly = HEIGHT * WIDTH + HEIGHT) { render.getRenderFunction() }
        verify(exactly = 2 * HEIGHT) { renderFunction.invoke("|") }
        verify(exactly = 2 * WIDTH - 4) { renderFunction.invoke("-") }
        verify(exactly = (HEIGHT * WIDTH) - (2 * WIDTH - 4) - (2 * HEIGHT)) { renderFunction.invoke(" ") }
        verify(exactly = HEIGHT) { renderFunction.invoke(System.lineSeparator()) }
    }

    @Test
    fun addPoint() {
        assertEquals(
            true,
            canvas.addPoint(Point(0, 0, "X")),
            "Wrong result on add point to position where already exist symbol"
        )
        assertEquals(
            false,
            canvas.addPoint(Point(1, 1, "X")),
            "Wrong result on add point to position where not exist symbol"
        )
        assertEquals(
            false,
            canvas.addPoint(Point(1, 1, "X")),
            "Wrong result on add point to position where already exist same symbol"
        )
    }

    companion object {
        private const val HEIGHT = 6
        private const val WIDTH = 22
    }
}