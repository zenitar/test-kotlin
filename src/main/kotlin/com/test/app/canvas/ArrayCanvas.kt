package com.test.app.canvas

import com.test.app.render.Render

/**
 * The array based canvas implementation.
 * @param width array width
 * @param height array height
 */
class ArrayCanvas(width: Int, height: Int, private val render: Render<Any>) : Canvas {
    private val width: Int = width + 2
    private val height: Int = height + 2

    /** The plain two-dimensional array. */
    private val canvas: Array<Array<String>> = Array(this.width) { Array(this.height) { " " } }

    /** {@inheritDoc} */
    override fun getWidth(): Int {
        return width
    }

    /** {@inheritDoc} */
    override fun getHeight(): Int {
        return height
    }

    /**
     * The code for all's constructors  methods.
     * Fill canvas borders.
     */
    init {
        fillHBorder()
        fillVBorder()
    }

    /** {@inheritDoc} */
    override fun renderCanvas() {
        for (h in 0 until height) {
            for (w in 0 until width)
                render.getRenderFunction()(canvas[w][h])
            render.getRenderFunction()(System.lineSeparator())
        }
    }

    /** {@inheritDoc} */
    override fun addPoint(point: Point): Boolean {
        if (canvas[point.x][point.y] == point.value || canvas[point.x][point.y] != "X" && canvas[point.x][point.y] != V_BORDER && canvas[point.x][point.y] != H_BORDER) {
            canvas[point.x][point.y] = point.value
            return false
        }

        return true
    }

    /** The method to fill right and left borders the {@link ArrayCanvas#H_BORDER} value. */
    private fun fillVBorder() {
        for (h in 0 until height) {
            canvas[0][h] = V_BORDER
            canvas[width - 1][h] = V_BORDER
        }
    }

    /** The method to fill top or bottom border the {@link ArrayCanvas#V_BORDER} value. */
    private fun fillHBorder() {
        for (w in 0 until width) {
            canvas[w][0] = H_BORDER
            canvas[w][height - 1] = H_BORDER
        }
    }

    /** {@inheritDoc} */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArrayCanvas

        if (width != other.width) return false
        if (height != other.height) return false
        if (!canvas.contentDeepEquals(other.canvas)) return false

        return true
    }

    /** {@inheritDoc} */
    override fun hashCode(): Int {
        var result = width
        result = 31 * result + height
        result = 31 * result + canvas.contentDeepHashCode()
        return result
    }

    /** The companion object for static code. */
    companion object {
        /** The right and left border value. */
        private const val V_BORDER = "|"
        /** The top and bottom border value. */
        private const val H_BORDER = "-"
    }
}