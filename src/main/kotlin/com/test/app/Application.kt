package com.test.app

import com.test.app.command.Command
import com.test.app.command.CommandFactory
import com.test.app.render.ConsoleRender
import com.test.app.render.Render

/** The main application class. */
class Application {
}

fun main(args: Array<String>) {
    var command: Command?
    val appContext = AppContext()
    appContext.addToContext(Render.CONTEXT_KEY, ConsoleRender())
    do {
        try {
            print("enter command: ")
            val curLine = readLine()!!
            command = CommandFactory.getCommandByName(curLine)
            command.execute(appContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    } while (true)
}