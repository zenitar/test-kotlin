package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.Canvas

/** The command to get canvas from context if exist and render it. */
interface RenderCommand : Command {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        appContext.getFromContext<Canvas>(Canvas.CONTEXT_KEY).renderCanvas()
    }
}