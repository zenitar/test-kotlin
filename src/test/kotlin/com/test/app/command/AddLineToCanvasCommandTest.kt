package com.test.app.command

import com.test.app.canvas.Point
import io.mockk.verify
import org.junit.Test

import org.junit.Before
import kotlin.test.assertEquals

class AddLineToCanvasCommandTest : RenderCommandTest() {
    lateinit var addLineToCanvasCommand: AddLineToCanvasCommand

    @Before
    fun setup() {
        addLineToCanvasCommand = AddLineToCanvasCommand("$NAME $X1 $Y1 $X2 $Y2")
    }

    @Test
    fun checkName() {
        assertEquals(NAME, AddLineToCanvasCommand.COMMAND_NAME, "The command console name doesn't match")
    }

    @Test
    fun execute() {
        addLineToCanvasCommand.execute(appContext)
        for (x in X1 until X2)
            for (y in Y1 until Y2)
                verify { mockCanvas.addPoint(Point(x, y,"X")) }
    }

    companion object {
        private const val X1 = 2
        private const val X2 = 6
        private const val Y1 = 2
        private const val Y2 = 4
        private const val NAME = "L"
    }
}