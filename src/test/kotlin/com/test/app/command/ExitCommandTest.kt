package com.test.app.command

import org.junit.Test
import kotlin.test.assertEquals


class ExitCommandTest {
    @Test
    fun testName() {
        assertEquals("Q", ExitCommand.COMMAND_NAME, "Exit command console name doesn't match")
    }
}