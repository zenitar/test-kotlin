package com.test.app.figure

import com.test.app.canvas.ArrayCanvas
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Test

import kotlin.math.abs

class RectangleTest {
    @MockK
    lateinit var canvas: ArrayCanvas

    init {
        MockKAnnotations.init(this)
    }

    @Test
    fun addToCanvas() {
        every { canvas.addPoint(any()) } returns false
        Rectangle(X1, Y1, X2, Y2).addToCanvas(canvas)
        verify(exactly = 2 * (abs(Y1 - Y2) + 1 + abs(X1 - X2) + 1)) { canvas.addPoint(any()) }
    }

    companion object {
        private const val X1 = 2
        private const val X2 = 6
        private const val Y1 = 2
        private const val Y2 = 4
    }
}