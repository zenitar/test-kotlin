package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.ArrayCanvas
import com.test.app.canvas.Canvas
import com.test.app.render.Render

/**
 * The command to create and add {@see Canvas} to {@see Render}.
 * @param inLine string command like 'C 2 2'.
 * Where the first number is canvas width, and the other is canvas height
 */
class CreateCanvasCommand(private val inLine: String) : RenderCommand {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        val args = getArgsFromCommandLine(inLine)
        appContext.addToContext(
                Canvas.CONTEXT_KEY,
                ArrayCanvas(args[1].toInt(), args[2].toInt(), appContext.getFromContext(Render.CONTEXT_KEY))
        )
        super.execute(appContext)
    }

    /** The companion object for static code. */
    companion object {
        /**The method to get console command name. */
        const val COMMAND_NAME: String = "C"
    }
}