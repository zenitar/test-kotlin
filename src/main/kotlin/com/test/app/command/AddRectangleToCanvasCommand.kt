package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.Canvas
import com.test.app.figure.Rectangle

/**
 * The command to create and add {@see Rectangle} to {@see Canvas}.
 * @param inLine string command like 'R 1 2 3 4'.
 * Where first two numbers are upper-left corner of rectangle, and other two are down-right corner of rectangle
 */
class AddRectangleToCanvasCommand(private val inLine: String) : RenderCommand {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        val args = getArgsFromCommandLine(inLine)
        Rectangle(args[1].toInt(), args[2].toInt(), args[3].toInt(), args[4].toInt())
                .addToCanvas(appContext.getFromContext(Canvas.CONTEXT_KEY))
        super.execute(appContext)
    }

    /** The companion object for static code. */
    companion object {
        /**The method to get console command name. */
        const val COMMAND_NAME: String = "R"
    }
}