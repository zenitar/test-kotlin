package com.test.app.figure

import com.test.app.canvas.Canvas

/** The interface to mark objects like figure. */
interface Figure {
    /** The method to add figure to {@link Canvas}. */
    fun addToCanvas(canvas: Canvas)
}