package com.test.app.figure

import com.test.app.canvas.Canvas

/**
 * The implementation of rectangle figure.
 * @param x1 x coords of rectangle upper-left corner point
 * @param y1 y coords of rectangle upper-left corner point
 * @param x2 x coords of rectangle down-right corner point
 * @param y2 y coords of rectangle down-right corner point
 */
class Rectangle(x1: Int, y1: Int, x2: Int, y2: Int) : Figure {
    /** Lines of rectangle. */
    private val lines: MutableList<Line> = ArrayList()

    /**
     * The code for all's constructors  methods.
     * Fill the lines list of rectangle lines.
     */
    init {
        lines.add(0, Line(x1, y1, x1, y2))
        lines.add(1, Line(x1, y2, x2, y2))
        lines.add(2, Line(x2, y2, x2, y1))
        lines.add(3, Line(x2, y1, x1, y1))
    }

    /** {@inheritDoc} */
    override fun addToCanvas(canvas: Canvas) {
        lines.forEach{line -> line.addToCanvas(canvas)}
    }

    /** {@inheritDoc} */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Rectangle

        if (lines != other.lines) return false

        return true
    }

    /** {@inheritDoc} */
    override fun hashCode(): Int {
        return lines.hashCode()
    }
}