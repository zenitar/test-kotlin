package com.test.app.command

import com.test.app.canvas.ArrayCanvas
import com.test.app.canvas.Canvas
import io.mockk.verify
import org.junit.Test

import org.junit.Before
import kotlin.test.assertEquals

class CreateCanvasCommandTest: RenderCommandTest() {
    lateinit var createCanvasCommand: CreateCanvasCommand

    @Before
    fun setup() {
        createCanvasCommand = CreateCanvasCommand("$NAME $W $H")
    }

    @Test
    fun checkName() {
        assertEquals(NAME, CreateCanvasCommand.COMMAND_NAME, "The command console name doesn't match")
    }

    @Test
    fun execute() {
        val canvas = ArrayCanvas(W + 2, H + 2, render)
        createCanvasCommand.execute(appContext)
        verify { appContext.addToContext(Canvas.CONTEXT_KEY, canvas) }
    }

    companion object {
        private const val NAME = "C"
    }
}