package com.test.app.render

import com.test.app.canvas.Canvas

/**
 * The implementation of {@link Render} to render objects to console.
 * @param canvas current render {@link Canvas}
 */
class ConsoleRender(private var canvas: Canvas? = null) : Render<String> {
    /** {@inheritDoc} */
    override fun getRenderFunction(): (String) -> Unit {
        return writer
    }

    /** The companion object for static code. */
    companion object {
        /** The closure to print given string to console. */
        val writer: (String) -> Unit = { s: String -> print(s) }
    }
}