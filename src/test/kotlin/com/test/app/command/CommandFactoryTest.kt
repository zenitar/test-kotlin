package com.test.app.command

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import kotlin.reflect.KClass
import kotlin.test.assertEquals


@RunWith(Parameterized::class)
class CommandFactoryTest(private val commandLine: String, private val command: KClass<out Command>) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun date(): Collection<Array<Any>> {
            return listOf(
                arrayOf("C 2 4", CreateCanvasCommand::class),
                arrayOf("R 2 4 2 4", AddRectangleToCanvasCommand::class),
                arrayOf("B 2 2 o", AddBucketFillToCanvasCommand::class),
                arrayOf("L 2 4 2 4", AddLineToCanvasCommand::class),
                arrayOf("Q", ExitCommand::class),
                arrayOf("c 2 4", CreateCanvasCommand::class),
                arrayOf("r 2 4 2 4", AddRectangleToCanvasCommand::class),
                arrayOf("b 2 2 o", AddBucketFillToCanvasCommand::class),
                arrayOf("l 2 4 2 4", AddLineToCanvasCommand::class),
                arrayOf("q", ExitCommand::class)
            )
        }
    }

    @Test
    fun getCommandByName() {
        assertEquals(
            command,
            CommandFactory.getCommandByName(commandLine)::class,
            "Wrong command got from command line"
        )
    }
}