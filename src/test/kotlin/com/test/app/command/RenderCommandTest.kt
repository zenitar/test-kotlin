package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.ArrayCanvas
import com.test.app.canvas.Canvas
import com.test.app.render.Render
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK

abstract class RenderCommandTest {
    @MockK
    lateinit var appContext: AppContext

    @MockK
    lateinit var render: Render<Any>

    @MockK
    lateinit var mockCanvas: ArrayCanvas

    init {
        MockKAnnotations.init(this)
        every { appContext.getFromContext<Render<Any>>(Render.CONTEXT_KEY) } returns render
        every { appContext.addToContext(any(), any()) } returns Unit
        every { appContext.getFromContext<Canvas>(Canvas.CONTEXT_KEY) } returns mockCanvas
        every { mockCanvas.renderCanvas() } returns Unit
        every { mockCanvas.addPoint(any()) } returns false
        every { render.getRenderFunction() } returns {}
    }

    companion object {
        const val W = 2
        const val H = 4
    }
}