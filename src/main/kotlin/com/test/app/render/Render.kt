package com.test.app.render


/** The base interface to mark some render. */
interface Render<T> {
    /** The method to get render function. */
    fun getRenderFunction(): (T) -> Unit

    /** The companion object for static code. */
    companion object{
        /** The key to set object to the app context. */
        const val CONTEXT_KEY: String = "render"
    }
}