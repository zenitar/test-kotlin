package com.test.app.canvas

/** The data class to describe the point. */
data class Point(val x: Int, val y: Int, val value: String)