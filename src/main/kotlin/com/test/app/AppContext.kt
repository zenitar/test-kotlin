package com.test.app

import java.lang.RuntimeException

/** The application context. */
open class AppContext {
    /** The map to contain context objects. */
    private val map: MutableMap<String, Any> = HashMap()

    /** The method to add object to the context. */
    fun addToContext(key: String, value: Any) {
        map[key] = value
    }

    /** The method to get object from the context. */
    @Suppress("UNCHECKED_CAST")
    fun <T> getFromContext(key: String?): T {
        try {
            return map[key!!]!! as T
        } catch (e: Exception) {
            throw RuntimeException("Failed to get $key from context", e)
        }
    }
}