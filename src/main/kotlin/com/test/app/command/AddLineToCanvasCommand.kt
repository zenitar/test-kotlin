package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.Canvas
import com.test.app.figure.Line

/**
 * The command to create and add {@see Line} to {@see Canvas}.
 * @param inLine string command like 'L 1 2 3 4'.
 * Where first two numbers are start line point, and other two are end line point
 */
class AddLineToCanvasCommand(private val inLine: String) : RenderCommand {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        val args = getArgsFromCommandLine(inLine)
        Line(args[1].toInt(), args[2].toInt(), args[3].toInt(), args[4].toInt())
                .addToCanvas(appContext.getFromContext(Canvas.CONTEXT_KEY))
        super.execute(appContext)
    }

    /** The companion object for static code. */
    companion object {
        /**The method to get console command name. */
        const val COMMAND_NAME: String = "L"
    }
}