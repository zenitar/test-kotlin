package com.test.app.command

import com.test.app.AppContext
import com.test.app.canvas.Canvas
import com.test.app.figure.BucketFill

/**
 * The command to create and add {@see BucketFill} to {@see Canvas}.
 * @param inLine string command like 'B 1 2 O'.
 * Where the numbers are coords where will be start to fill 'O' symbols
 */
class AddBucketFillToCanvasCommand(private val inLine : String) : RenderCommand {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        val args = getArgsFromCommandLine(inLine)
        val canvas = appContext.getFromContext<Canvas>(Canvas.CONTEXT_KEY)
        BucketFill(args[1].toInt(), args[2].toInt(), args[3])
                .addToCanvas(canvas)
        super.execute(appContext)
    }

    /** The companion object for static code. */
    companion object {
        /**The method to get console command name. */
        const val COMMAND_NAME: String = "B"
    }
}