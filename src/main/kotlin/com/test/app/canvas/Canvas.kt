package com.test.app.canvas


/** The basic canvas interface. */
interface Canvas {
    /**
     * The method to get canvas width.
     * @return integer value of canvas width
     */
    fun getWidth(): Int

    /**
     * The method to get canvas height.
     * @return integer value of canvas height
     */
    fun getHeight(): Int

    /** The method to add {@see Point} to canvas. */
    fun addPoint(point: Point): Boolean

    /** The method to render canvas. */
    fun renderCanvas()

    /** The companion object for static code. */
    companion object {
        /** The key to set object to the app context. */
        const val CONTEXT_KEY: String = "canvas"
    }
}