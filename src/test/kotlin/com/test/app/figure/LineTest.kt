package com.test.app.figure

import com.test.app.canvas.ArrayCanvas
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.Test

import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class LineTest(
    private val x1: Int,
    private val y1: Int,
    private val x2: Int,
    private val y2: Int,
    private val times: Int
) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun date(): Collection<Array<Int>> {
            return listOf(
                arrayOf(2, 2, 4, 2, 3),
                arrayOf(2, 2, 2, 4, 3),
                arrayOf(2, 2, 2, 2, 1),
                arrayOf(4, 2, 2, 2, 3),
                arrayOf(2, 4, 2, 2, 3)
            )
        }
    }

    init {
        MockKAnnotations.init(this)
    }

    @MockK
    lateinit var canvas: ArrayCanvas

    @Test
    fun addToCanvas() {
        every { canvas.addPoint(any()) } returns false
        Line(x1, y1, x2, y2).addToCanvas(canvas)
        verify(exactly = times) { canvas.addPoint(any()) }
    }
}