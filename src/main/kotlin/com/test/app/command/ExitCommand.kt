package com.test.app.command

import com.test.app.AppContext
import kotlin.system.exitProcess

/** The exit command implementation. */
class ExitCommand : Command {
    /** {@inheritDoc} */
    override fun execute(appContext: AppContext) {
        exitProcess(0)
    }

    /** The companion object for static code. */
    companion object {
        /**The method to get console command name. */
        const val COMMAND_NAME: String = "Q"
    }
}