package com.test.app.command

import com.test.app.canvas.Point
import io.mockk.verify
import org.junit.Test

import org.junit.Before
import kotlin.test.assertEquals

class AddRectangleToCanvasCommandTest: RenderCommandTest() {
    lateinit var addRectangleToCanvasCommand: AddRectangleToCanvasCommand

    @Before
    fun setup() {
        addRectangleToCanvasCommand = AddRectangleToCanvasCommand("$NAME $X1 $Y1 $X2 $Y2")
    }

    @Test
    fun checkName() {
        assertEquals(NAME, AddRectangleToCanvasCommand.COMMAND_NAME,"The command console name doesn't match")
    }

    @Test
    fun execute() {
        addRectangleToCanvasCommand.execute(appContext)
        for ( x in X1 until X2) {
            verify { mockCanvas.addPoint(Point(x, Y1, "X")) }
            verify { mockCanvas.addPoint(Point(x, Y2, "X")) }
        }
        for (y in Y1 until Y2) {
            verify { mockCanvas.addPoint(Point(X1, y, "X")) }
            verify { mockCanvas.addPoint(Point(X2, y, "X")) }
        }
    }

    companion object {
        private const val X1 = 2
        private const val X2 = 6
        private const val Y1 = 2
        private const val Y2 = 4
        private const val NAME = "R"
    }
}