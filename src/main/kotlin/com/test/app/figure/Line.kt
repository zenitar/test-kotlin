package com.test.app.figure

import com.test.app.canvas.Canvas
import com.test.app.canvas.Point
import kotlin.math.max
import kotlin.math.min

/**
 * The line figure implementation.
 * @param x1 x coords of start line point
 * @param y1 y coords of start line point
 * @param x2 x coords of end lone point
 * @param y2 y coords of end line point
 */
class Line(private val x1: Int, private val y1: Int, private val x2: Int, private val y2: Int) : Figure {
    /** {@inheritDoc} */
    override fun addToCanvas(canvas: Canvas) {
        val minX = min(x1, x2);
        val maxX = max(x1, x2);
        val minY = min(y1, y2);
        val maxY = max(y1, y2);

        if (minX == maxX && minY == maxY) {
            canvas.addPoint(Point(minX, minY, POINT_VALUE))
            return
        }

        if (minX == maxX) {
            for (y in minY until maxY + 1)
                canvas.addPoint(Point(minX, y, POINT_VALUE))
            return
        }

        if (maxY == minY) {
            for (x in minX until maxX + 1)
                canvas.addPoint(Point(x, minY, POINT_VALUE))
            return
        }

        for (y in minY until maxY + 1)
            for (x in minX until maxX + 1)
                canvas.addPoint(Point(x, y, POINT_VALUE))
    }

    /** {@inheritDoc} */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Line

        if (x1 != other.x1) return false
        if (y1 != other.y1) return false
        if (x2 != other.x2) return false
        if (y2 != other.y2) return false

        return true
    }

    /** {@inheritDoc} */
    override fun hashCode(): Int {
        var result = x1
        result = 31 * result + y1
        result = 31 * result + x2
        result = 31 * result + y2
        return result
    }

    /** The companion object for static code. */
    companion object {
        /** The one point of line value to draw on {@link Canvas}. */
        private const val POINT_VALUE = "X";
    }
}