package com.test.app.command

/**The factory to get command by it console name. */
class CommandFactory {
    /** The companion object for static code. */
    companion object {
        /** The map of registered commands and their names. */
        private val commands = mapOf<String, (String) -> Command>(
            Pair(CreateCanvasCommand.COMMAND_NAME, { inLine -> CreateCanvasCommand(inLine) }),
            Pair(ExitCommand.COMMAND_NAME, { _ -> ExitCommand() }),
            Pair(AddLineToCanvasCommand.COMMAND_NAME, { inLine -> AddLineToCanvasCommand(inLine) }),
            Pair(AddRectangleToCanvasCommand.COMMAND_NAME, { inLine -> AddRectangleToCanvasCommand(inLine) }),
            Pair(AddBucketFillToCanvasCommand.COMMAND_NAME, { inLine -> AddBucketFillToCanvasCommand(inLine) })
        )

        /**
         * The method to get {@see Command} by console string.
         * @param line string line from console
         * @throws RuntimeException if failed to find command.
         * @throws NullPointerException if {@param line} is null
         * @return the command
         */
        fun getCommandByName(line: String?): Command {
            return commands[getCommandName(line)]?.invoke(line!!)
                ?: throw RuntimeException("Unsupported command '${getCommandName(line)}'")
        }

        /**
         * The method to get console command name from console string line.
         * @param stringCommand string line from console
         * @return the first symbol of consoles string line
         * @throws NullPointerException if {@param stringCommand} is null
         */
        private fun getCommandName(stringCommand: String?): String {
            return stringCommand!!.substring(0, 1).toUpperCase()
        }
    }
}